FROM ubuntu

RUN apt-get install software-properties-common -y;\
    add-apt-repository ppa:ecometrica/servers;

RUN apt-get update && apt-get upgrade -y ;\
    apt-get install nodejs npm git python build-essential wget screen tmux curl vim -y;\
    ln -s /usr/bin/nodejs /usr/bin/node;

RUN apt-get install -y build-essential xorg libssl-dev libxrender-dev wget gdebi;
RUN apt-get 
RUN wget http://download.gna.org/wkhtmltopdf/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb;
RUN gdebi --n wkhtmltox-0.12.2.1_linux-trusty-amd64.deb;

COPY /handlers  /app/handlers
COPY /*.js /app/
COPY /package.json /app/

WORKDIR /app

RUN ls /app

RUN npm install

EXPOSE 3000

CMD ["node", "/app/server.js"]